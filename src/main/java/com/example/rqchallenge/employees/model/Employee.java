package com.example.rqchallenge.employees.model;

public class Employee {
	private int id;
	private String employee_name;
	private int employee_salary;
	private String employee_age;
	private String profile_image;
	
	
	public Employee(int id, String employee_name, int employee_salary, String employee_age,
			String profile_image) {
		super();
		this.id = id;
		this.employee_name = employee_name;
		this.employee_salary = employee_salary;
		this.employee_age = employee_age;
		this.profile_image = profile_image;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the employee_name
	 */
	public String getEmployee_name() {
		return employee_name;
	}


	/**
	 * @param employee_name the employee_name to set
	 */
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}


	/**
	 * @return the employee_salary
	 */
	public int getEmployee_salary() {
		return employee_salary;
	}


	/**
	 * @param employee_salary the employee_salary to set
	 */
	public void setEmployee_salary(int employee_salary) {
		this.employee_salary = employee_salary;
	}


	/**
	 * @return the employee_age
	 */
	public String getEmployee_age() {
		return employee_age;
	}


	/**
	 * @param employee_age the employee_age to set
	 */
	public void setEmployee_age(String employee_age) {
		this.employee_age = employee_age;
	}


	/**
	 * @return the profile_image
	 */
	public String getProfile_image() {
		return profile_image;
	}


	/**
	 * @param profile_image the profile_image to set
	 */
	public void setProfile_image(String profile_image) {
		this.profile_image = profile_image;
	}


	@Override
	public String toString() {
		return "Employee [id=" + String.valueOf(id) + ", employee_name=" + employee_name + ", employee_salary=" + String.valueOf(employee_salary)
				+ ", employee_age=" + employee_age + ", profile_image=" + profile_image + "]";
	}
	
	

	
	
	
}	
	