package com.example.rqchallenge.employees.repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.example.rqchallenge.employees.model.Employee;

// TODO: Auto-generated Javadoc
/**
 * The Class EmployeeRepository.
 */
@Repository
public class EmployeeRepository {

/** The employee list. */
private List<Employee> employeeList = new ArrayList<>();
	
	/**
	 * Adds the.
	 *
	 * @param employee the employee
	 * @return the employee
	 */
	public Employee add(Employee employee) {
		employeeList.add(employee);
		return employee;
	}

	/**
	 * Gets the employee by id.
	 *
	 * @param id the id
	 * @return the employee by id
	 */
	public Employee getEmployeeById(int id) {
		
		for(Employee emp :employeeList) {
			if(emp.getId()==id){
				return emp;
			}		
		}
		return null;
	}

	/**
	 * Delete employee by id.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean deleteEmployeeById(int id) {
		
		return employeeList.removeIf(e-> e.getId()==id);
	
		/*
		 * boolean empDeleted=false ; for(Employee emp :employeeList) {
		 * if(emp.getId()==id){ empDeleted= employeeList.remove(emp);
		 * 
		 * } } return empDeleted;
		 */
		
		
	}

	/**
	 * Gets the employee by name.
	 *
	 * @param searchString the search string
	 * @return the employee by name
	 */
	public List<Employee> getEmployeeByName(String searchString) {
		
		List<Employee> empList= new ArrayList<>() ;
		employeeList.forEach(e ->{
			Employee emp=null;
			if(e.getEmployee_name().contains(searchString)) {
				emp=e;
			}
			if(emp!=null)empList.add(emp);
		});
		return empList;
	}

	/**
	 * Gets the highest salary of employees.
	 *
	 * @return the highest salary of employees
	 */
	public Integer getHighestSalaryOfEmployees() {
		List<Employee> empList=employeeList.stream().sorted(Comparator.comparing(Employee::getEmployee_salary).reversed()).collect(Collectors.toList());
		
		return empList.get(0).getEmployee_salary();
	}

	/**
	 * Gets the top ten highest salary of employees.
	 *
	 * @return the top ten highest salary of employees
	 */
	public List<String> getTopTenHighestSalaryOfEmployees() {
		List<String> empNameList=employeeList.stream().sorted(Comparator.comparing(Employee::getEmployee_salary).reversed()).limit(10).map(Employee::getEmployee_name).collect(Collectors.toList());
		return empNameList;
	}

	public List<Employee> getAllEmployee() {
		// TODO Auto-generated method stub
		return employeeList;
	}
	
	
}
