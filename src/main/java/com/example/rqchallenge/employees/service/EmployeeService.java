package com.example.rqchallenge.employees.service;


import org.springframework.stereotype.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.rqchallenge.Exception.ResourceNotfoundException;
import com.example.rqchallenge.employees.model.Employee;
import com.example.rqchallenge.employees.repository.EmployeeRepository;


/**
 * The Class EmployeeService.
 */
@Service
public class EmployeeService {

	/** The emp repository. */
	@Autowired
	EmployeeRepository empRepository;
	
	/**
	 * Creates the employee.
	 *
	 * @param id the id
	 * @param empName the emp name
	 * @param empSalary the emp salary
	 * @param empAge the emp age
	 * @return the employee
	 */
	public Employee createEmployee(int id,String empName, int empSalary, String empAge) {
		
		if(empRepository.getEmployeeById(id)!=null) {
			throw new ResourceNotfoundException("employee already exsist for  id : "+id);
		}
		
		Employee employee=new Employee(id,empName,empSalary,empAge,"");
		
		
		return empRepository.add(employee);}

	/**
	 * Gets the employee by id.
	 *
	 * @param id the id
	 * @return the employee by id
	 */
	public Employee getEmployeeById(int id) {
		Employee emp=empRepository.getEmployeeById(id);
		if(emp==null) {
			throw new ResourceNotfoundException("employee not found for id : "+id);
		}
		
		return emp;
	}

	/**
	 * Delete employee by id.
	 *
	 * @param id the id
	 * @return the string
	 */
	public String deleteEmployeeById(int id) {
		boolean flag=empRepository.deleteEmployeeById(id);
		if(flag) {
			return "successfully! deleted Record";
		}else
		   return "record not found";
	}

	/**
	 * Gets the employee by name.
	 *
	 * @param searchString the search string
	 * @return the employee by name
	 */
	public List<Employee> getEmployeeByName(String searchString) {
		
		return empRepository.getEmployeeByName(searchString);
	}

	/**
	 * Gets the highest salary of employees.
	 *
	 * @return the highest salary of employees
	 */
	public Integer getHighestSalaryOfEmployees() {
		
		return empRepository.getHighestSalaryOfEmployees();
	}

	/**
	 * Gets the top ten highest salary of employees.
	 *
	 * @return the top ten highest salary of employees
	 */
	public List<String> getTopTenHighestSalaryOfEmployees() {
		
		return empRepository.getTopTenHighestSalaryOfEmployees();
	}

	/**
	 * Gets the all employee.
	 *
	 * @return the all employee
	 */
	public List<Employee>  getAllEmployee() {
		
		return empRepository.getAllEmployee();
	}
}
