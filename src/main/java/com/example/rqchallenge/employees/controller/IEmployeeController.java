package com.example.rqchallenge.employees.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.rqchallenge.employees.model.Employee;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RequestMapping("/default")
public interface IEmployeeController {

    @GetMapping()
    ResponseEntity<Object> getAllEmployees() throws IOException;

    @GetMapping("/search/{searchString}")
    ResponseEntity<Object> getEmployeesByNameSearch(@PathVariable String searchString);

    @GetMapping("/{id}")
    ResponseEntity<Object> getEmployeeById(@PathVariable String id);

    @GetMapping("/highestSalary")
    ResponseEntity<Object> getHighestSalaryOfEmployees();

    @GetMapping("/topTenHighestEarningEmployeeNames")
    ResponseEntity<Object> getTopTenHighestEarningEmployeeNames();

    @PostMapping("/create")
    ResponseEntity<Object> createEmployee(@RequestBody Map<String, Object> employeeInput);

    @DeleteMapping("/{id}")
    ResponseEntity<Object> deleteEmployeeById(@PathVariable String id);

}
