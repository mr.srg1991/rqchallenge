package com.example.rqchallenge.employees.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.rqchallenge.Exception.MissingRequestParameterException;
import com.example.rqchallenge.Response.CostomResponse;
import com.example.rqchallenge.employees.model.Employee;
import com.example.rqchallenge.employees.service.EmployeeService;

// TODO: Auto-generated Javadoc
/**
 * The Class EmployeeController.
 */
@RestController
@RequestMapping("/v1")
public class EmployeeController implements IEmployeeController {
	
	/** The Emp service. */
	@Autowired
	EmployeeService EmpService;

	/**
	 * Gets the all employees.
	 *
	 * @return the all employees
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public ResponseEntity<Object> getAllEmployees() throws IOException {
	
		return CostomResponse.generateResponse(HttpStatus.OK, EmpService.getAllEmployee());
	}

	/**
	 * Gets the employees by name search.
	 *
	 * @param searchString the search string
	 * @return the employees by name search
	 */
	@Override
	public ResponseEntity<Object> getEmployeesByNameSearch(String searchString) {
		
		return CostomResponse.generateResponse(HttpStatus.OK,EmpService.getEmployeeByName(searchString));
	}

	/**
	 * Gets the employee by id.
	 *
	 * @param id the id
	 * @return the employee by id
	 * @throws RuntimeException the runtime exception
	 */
	@Override
	public ResponseEntity<Object> getEmployeeById(String id) throws RuntimeException{
		int empId =Integer.valueOf(id);
		
		return CostomResponse.generateResponse(HttpStatus.OK,EmpService.getEmployeeById(empId));
	}

	/**
	 * Gets the highest salary of employees.
	 *
	 * @return the highest salary of employees
	 */
	@Override
	public ResponseEntity<Object> getHighestSalaryOfEmployees() {
		
		return CostomResponse.generateResponse(HttpStatus.OK,EmpService.getHighestSalaryOfEmployees());
	}

	/**
	 * Gets the top ten highest earning employee names.
	 *
	 * @return the top ten highest earning employee names
	 */
	@Override
	public ResponseEntity<Object> getTopTenHighestEarningEmployeeNames() {
		
		return CostomResponse.generateResponse(HttpStatus.OK,EmpService.getTopTenHighestSalaryOfEmployees());
	}

	/**
	 * Creates the employee.
	 *
	 * @param employeeInput the employee input
	 * @return the response entity
	 */
	@Override
	public ResponseEntity<Object> createEmployee(Map<String, Object> employeeInput) {
		
		if(!employeeInput.containsKey("id")|| !employeeInput.containsKey("name")||!employeeInput.containsKey("salary")||!employeeInput.containsKey("age")) {
			throw new MissingRequestParameterException("Missing Request Parameter");
		}
		int empId =Integer.valueOf((String) employeeInput.get("id"));
		String empName =(String)employeeInput.get("name");
		int empSalary =Integer.valueOf((String) employeeInput.get("salary"));;
		String empAge =(String)employeeInput.get("age");
		
		
		return CostomResponse.generateResponse(HttpStatus.OK,EmpService.createEmployee(empId,empName,empSalary,empAge));
	}
	
	/**
	 * Delete employee by id.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@Override
	public ResponseEntity<Object> deleteEmployeeById(String id) {
		int empId =Integer.valueOf(id);
		return CostomResponse.generateResponse(HttpStatus.OK,EmpService.deleteEmployeeById(empId));
	}

}
