package com.example.rqchallenge.Exception;

public class ResourceAlreadyExistedException extends RuntimeException {

	  private static final long serialVersionUID = 1L;

	  public ResourceAlreadyExistedException(String msg) {
	    super(msg);
	  }
}
