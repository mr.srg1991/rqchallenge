package com.example.rqchallenge.Exception;

public class ResourceNotfoundException extends RuntimeException {

	  private static final long serialVersionUID = 1L;

	  public ResourceNotfoundException(String msg) {
	    super(msg);
	  }
}
