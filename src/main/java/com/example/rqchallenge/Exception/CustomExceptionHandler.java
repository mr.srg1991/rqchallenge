package com.example.rqchallenge.Exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomExceptionHandler.
 */
@RestControllerAdvice
public class CustomExceptionHandler {

	 /**
 	 * Resource not found exception.
 	 *
 	 * @param ex the ex
 	 * @param request the request
 	 * @return the response entity
 	 */
 	@ExceptionHandler(ResourceNotfoundException.class)
	  public ResponseEntity<ErrorDetails> resourceNotFoundException(ResourceNotfoundException ex, WebRequest request) {
		 ErrorDetails message = new ErrorDetails(
	        HttpStatus.NOT_FOUND.value(),
	        new Date(),
	        ex.getMessage(),
	        request.getDescription(false));

	    return new ResponseEntity<ErrorDetails>(message, HttpStatus.NOT_FOUND);
	  }

	  /**
  	 * Global exception handler.
  	 *
  	 * @param ex the ex
  	 * @param request the request
  	 * @return the response entity
  	 */
  	@ExceptionHandler(Exception.class)
	  public ResponseEntity<ErrorDetails> globalExceptionHandler(Exception ex, WebRequest request) {
		  ErrorDetails message = new ErrorDetails(
	        HttpStatus.INTERNAL_SERVER_ERROR.value(),
	        new Date(),
	        ex.getMessage(),
	        request.getDescription(false));

	    return new ResponseEntity<ErrorDetails>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	  }
	  
	  
	  /**
  	 * Request parameter not found exception.
  	 *
  	 * @param ex the ex
  	 * @param request the request
  	 * @return the response entity
  	 */
  	@ExceptionHandler(MissingRequestParameterException.class)
	  public ResponseEntity<ErrorDetails> requestParameterNotFoundException(MissingRequestParameterException ex, WebRequest request) {
		 ErrorDetails message = new ErrorDetails(
	        HttpStatus.NOT_FOUND.value(),
	        new Date(),
	        ex.getMessage(),
	        request.getDescription(false));

	    return new ResponseEntity<ErrorDetails>(message, HttpStatus.NOT_FOUND);
	  }
	  
	  
	  
	  /**
  	 * Resource already existed exception.
  	 *
  	 * @param ex the ex
  	 * @param request the request
  	 * @return the response entity
  	 */
  	@ExceptionHandler(ResourceAlreadyExistedException.class)
	  public ResponseEntity<ErrorDetails> resourceAlreadyExistedException(ResourceAlreadyExistedException ex, WebRequest request) {
		 ErrorDetails message = new ErrorDetails(
	        HttpStatus.NOT_FOUND.value(),
	        new Date(),
	        ex.getMessage(),
	        request.getDescription(false));

	    return new ResponseEntity<ErrorDetails>(message, HttpStatus.NOT_FOUND);
	  }
}
