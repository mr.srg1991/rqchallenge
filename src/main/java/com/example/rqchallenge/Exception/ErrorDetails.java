package com.example.rqchallenge.Exception;

import java.util.Date;

public class ErrorDetails {
	private int  status;
	private Date timestamp;
	private String message;
	private String details;
	
	
	
	/**
	 * @param i 
	 * @param timestamp
	 * @param message
	 * @param details
	 */
	public ErrorDetails(int status, Date timestamp, String message, String details) {
		super();
		this.status=status;
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}



	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}



	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}



	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}



	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}



	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}



	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}



	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	

}
