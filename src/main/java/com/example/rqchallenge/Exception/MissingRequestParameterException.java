package com.example.rqchallenge.Exception;

public class MissingRequestParameterException extends RuntimeException {

	  private static final long serialVersionUID = 1L;

	  public MissingRequestParameterException(String msg) {
	    super(msg);
	  }
}
