package com.example.rqchallenge.Response;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CostomResponse {
	
	public static ResponseEntity<Object> generateResponse( HttpStatus status, Object responseObj) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "success");
		if(responseObj instanceof String) {
			
			map.put("message", responseObj);
		}
		
		map.put("data", responseObj);
		
		return new ResponseEntity<Object>(map, status);
	}
}
